rm -rf /opt/ANDRAX/amass

cd cmd/amass

go build ./...

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Build amass... PASS!"
else
  # houston we have a problem
  exit 1
fi

strip amass

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Strip amass... PASS!"
else
  # houston we have a problem
  exit 1
fi

mkdir -p /opt/ANDRAX/amass/bin

cp -Rf amass /opt/ANDRAX/amass/bin

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

cd ../../

cp -Rf examples/config.yaml /opt/ANDRAX/amass
cp -Rf examples/wordlists /opt/ANDRAX/amass

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE resources... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/amass /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX

rm -rf /root/go
rm -rf /home/mythical/go
